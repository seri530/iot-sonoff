/*
- Setup wifi connection
- Setup HTTP Server
- Setup MQTT Server
*/

#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <ESP8266WiFi.h>

#include <Console.cpp>
#include <Parts\Pin.h>
#include <Parts\Led.h>
#include <Parts\Relay.h>
#include <Parts\Button.h>

#include <http_server.cpp>

using Parts::Button;
using Parts::Led;
using Parts::Relay;
using Parts::Pin;

Led *led;
Relay *relay;
Button *button;

//--------------------------------------------------------------------------------

#include <ESP8266WebServer.h>

ESP8266WebServer *server;

void handleRootPath()
{
    server->send(200, "text/plain", "Hello world");
}

void handleDevice()
{
    String response = "{interfaces:[";
    response += "{name: 'switch1',type: 'switch', functions: ['on', 'off', 'toggle','']}";
    response += " ]}";
    server->send(200, "application/json", response);
}

void handleSwitch1()
{
    server->send(200, "application/json", relay->getValue() ? "true" : "false");
}

void onSwitch1()
{
    relay->on();
    server->send(200, "application/json", "true");
}

void offSwitch1()
{
    relay->off();
    server->send(200, "application/json", "false");
}

void toggleSwitch1()
{
    relay->toggle();
    server->send(200, "application/json", relay->getValue() ? "true" : "false");
}

void set_http_server()
{
    server = new ESP8266WebServer(80);
    server->on("/", handleRootPath);
    server->on("/device", handleDevice);
    server->on("/switch1", handleSwitch1);
    server->on("/switch1/on", onSwitch1);
    server->on("/switch1/off ", offSwitch1);
    server->on("/switch1/toggle", toggleSwitch1);

    server->begin();
}

//--------------------------------------------------------------------------------

void init_wifi()
{
    WiFi.mode(WIFI_STA);
    if (WiFi.getAutoConnect() == false)
    {
        WiFi.setAutoConnect(true);
    }
    WiFi.setAutoReconnect(true);

    // start WiFi connecting
    int cnt = 0;

    led->setValue(128);
    led->persist();

    while (WiFi.status() != WL_CONNECTED)
    {

        delay(500);

        // if no connection available after timeout
        if (cnt++ >= 40) //20s
        {
            Console::println("SmartConfg");
            WiFi.beginSmartConfig();
            while (1)
            {
                delay(125);

                if (led->getValue() > 1000)
                    led->off();
                else
                    led->on();
                led->persist();

                if (WiFi.smartConfigDone())
                {
                    Console::println(F("[WIFI] SmartConfig: Success"));
                    break;
                }
            }
        }
        led->off();
        led->persist();
    }

    Console::println(F("WiFi connected"));
}

void setup()
{
    Console::Init();
    led = new Led(new Pin(13, true));
    relay = new Relay(new Pin(12, true));
    button = new Button(0);

    init_wifi();
    set_http_server();
}

void loop()
{
    Console::println("EDDIG OK");
    server->handleClient();
    Console::println("SERVER OK");

    if (button->isDown())
    {
        Console::println("Is Down");
        led->getValue() > 0 ? led->off() : led->on();
        relay->toggle();
    }

    led->persist();
    relay->persist();

    delay(100);
}
