#include <Arduino.h>

namespace
{
class Console
{
  public:
    static void Init();
    static void println(String text);
    static void print(String text);
    static void print(float num);

  private:
    static bool isInicialized;
};

bool Console::isInicialized = false;

void Console::Init()
{
    if (!isInicialized)
    {
        isInicialized = true;
        Serial.begin(9600);
    }
}

void Console::println(String text)
{
    Serial.println(text);
}

void Console::print(String text)
{
    Serial.print(text);
}

void Console::print(float num)
{
    Serial.print(num);
}
}